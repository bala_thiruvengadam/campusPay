var schedule = require('node-schedule');
var dbUtil = require('../config/dbUtil');
var affsSvc = require('../affiliates/affiliatesSvc');
var request = require('request-promise');
var jobSvc = require('./jobLogSvc');
var async = require('async');
var dateutil = require('dateutil');
var affiliatesOrderSvc = require("../affiliates_orders/orderSvc");
var conversionSvc = require("../convertors/conversionSvc");


var getOneMonthBackdate = function(date) {
	date.setHours(0, 0, 0, 0);
	var prevMnt = date.getMonth() - 1;
	date.setMonth(prevMnt);
	return date;
}

var getReportURL = function(aff, jobObj, status, offset) {
	var reportsAPI = aff.apis['reports'];
	var url = reportsAPI.url + "?status=" + status + "&startDate=" + dateutil.format(jobObj.start_date, 'Y-m-d') + "&endDate=" + dateutil.format(jobObj.end_date, 'Y-m-d') + "&offset=" + offset || 0;
//var url = reportsAPI.url + "?status=" + status + "&startDate=2017-01-01&endDate=2017-09-07&offset=" + offset || 0;

	console.log("build URL = ", url);
	return url;
};

var initFKReport = function(status, headers, reportURL, callback) {

	var reqOptions = {
		method: 'GET',
		uri: reportURL,
		headers: headers,
		json: true
	};
	request(reqOptions).then((response) => {

		var orders = response.orderList; 
		console.log("OrderCount for Status ",status, " Count is",orders.length);
		var update_date = new Date();
		var newOrders = [];
		async.map(orders, (order, callback) => {
			newOrders.push(conversionSvc.convertFLIPKARTOrder(order, "FLIPKART", status));
			callback();
		}, function(err, modArrays) {
			orders = newOrders;
			//console.log("Orders :: ", orders);

			if (err) {
				throw err;
			} else if (orders.length == 0) {
				callback();
			} else {

				/*if (status == "tentative") {
					// resolve earlier
					async.each(orders, function(order, callback2) {
						affiliatesOrderSvc.saveOrder(order).then((order) => {
							callback2();
						}).catch((err) => {
							throw err;
						});
					}, function(err, result) {
						if (!err) {
							if (response.next == '') {
								callback();
							} else {
								initFKReport(status, headers, response.next, callback);
							}
						} else {
							throw err;
						}
					});

				} else {*/
					dbUtil.getConnection((db) => {
						var tableName = "affiliates_" + orders[0].affiliateId + "_orders";
						console.log("Table name .. ", tableName);
						var collection = db.collection(tableName);
						var bulk = collection.initializeUnorderedBulkOp();

						for (var idx = 0; idx < orders.length; idx++) {
							var order = orders[idx];
							bulk.find({
								affiliateId: order.affiliateId,
								productId: order.productId,
								orderId: order.orderId
							}).upsert().updateOne(order);
						}
						bulk.execute(function(err, resData) {
							if (!err) {
								if (response.next == '') {
									callback();
								} else {
									initFKReport(status, headers, response.next, callback);
								}
							} else {
								throw err;
							}

						});

					});
				//}
			}

		});

	}).catch((err) => {
		throw err;
	});
}

var FKCnfReports = function() {
	var jobName = "fk_report_confirmed";
	var current_date = new Date();
	console.log("Scheduling fk_report_confirmed jobs");
	jobSvc.findJob({
		name: jobName,
		status: "completed"
	}, {
		sort: [
			["created_on", -1]
		]
	}).then((job) => {
		var endDate = getOneMonthBackdate(current_date);
		var startDate;
		if (!job) {
			startDate = new Date(2017, 00, 01);
		} else {
			startDate = job.end_date;
		}

		var jobObj = {
			start_date: startDate,
			end_date: endDate,
			name: "fk_report_confirmed",
			status: "started",
			created_on: current_date
		};
		jobSvc.insertNewJob(jobObj).then((jobObj) => {
			affsSvc.findOne({affiliateId:"FLIPKART"}).then((aff) => {
				var headers = {
					"Fk-Affiliate-Id": aff.trk_id,
					"Fk-Affiliate-Token": aff.token
				};
				async.each(["approved", "cancelled", "disapproved"], function(status, callback) {
					var offset = 0;
					var reportURL = getReportURL(aff, jobObj, status, offset);
					initFKReport(status, headers, reportURL, callback);

				}, function(err, results) {
					if (err) {
						console.log("err", err);
						throw err;
					} else {
						jobObj.status = "completed";
						jobSvc.updateJob({
							_id: jobObj._id
						}, jobObj).then((result) => {
							console.log("Job complete on ", new Date());
						});
					}
				});

			}).catch((err) => {
				jobObj.status = "failed";
				jobSvc.updateJob({
					_id: jobObj._id
				}, jobObj).then((result) => {
					console.log("Job failed ", jobObj.name, new Date());
				});
			});
		});



	});



};


/**
	This job will run every day
**/
var FKTentativeReports = function() {
	var jobName = "fk_report_tentative";
	console.log("Scheduling fk_report_tentative jobs");
	
	var current_date = new Date();
	jobSvc.findJob({
		name: "fk_report_tentative",
		status: "completed"
	}, {
		sort: [
			["created_on", -1]
		]
	}).then((job) => {
		var endDate = current_date;
		var startDate;
		if (!job) {
			startDate = new Date(2017, 00, 01);
		} else {
			startDate = job.end_date;
			//startDate.
		}

		var jobObj = {
			start_date: startDate,
			end_date: endDate,
			name: jobName,
			status: "started",
			created_on: current_date
		};
		jobSvc.insertNewJob(jobObj).then((jobObj) => {
			affsSvc.findOne({affiliateId:"FLIPKART"}).then((aff) => {
				var headers = {
					"Fk-Affiliate-Id": aff.trk_id,
					"Fk-Affiliate-Token": aff.token
				};
				async.each(["tentative"], function(status, callback) {
					var offset = 0;
					var reportURL = getReportURL(aff, jobObj, status, offset);
					initFKReport(status, headers, reportURL, callback);

				}, function(err, results) {
					if (err) {
						console.log("err", err);
						throw err;
					} else {
						jobObj.status = "completed";
						jobSvc.updateJob({
							_id: jobObj._id
						}, jobObj).then((result) => {
							console.log("[ Job ][", jobObj.name, "] completed on", new Date());
						});
					}
				});

			}).catch((err) => {
				jobObj.status = "failed";
				jobSvc.updateJob({
					_id: jobObj._id
				}, jobObj).then((result) => {
					console.log("[ Job ][", jobObj.name, "] failed on", new Date());
				});
			});
		});



	});



};

module.exports.launchJobs = function() {
	schedule.scheduleJob('0 */10 * * *', FKCnfReports());
	schedule.scheduleJob('0 */10 * * *', FKTentativeReports());
};