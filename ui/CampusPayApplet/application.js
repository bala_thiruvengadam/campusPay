$(document).ready(function() {
	var offerBoxCSS = "col-xs-6 col-sm-6 col-md-6 col-lg-6 offer-box";
	var offerBoxNew = "col-xs-12 col-sm-12 col-md-12 col-lg-12 offer-box";
	var offerSummaryOld = "col-xs-12 col-sm-12 col-md-12 col-lg-12 offer-summary";
	var offerSummaryNew = "col-xs-6 col-sm-6 col-md-6 col-lg-6 offer-summary";
	var offerDetailsOld = "col-xs-12 col-sm-12 col-md-12 col-lg-12 offer-details";
	var offerDetailsNew = "col-xs-6 col-sm-6 col-md-6 col-lg-6 offer-details";
	var _expandand = false;
	$(".offer-details-link").click(function(ev) {
		ev.preventDefault();
		ev.stopPropagation();

		var $offerContainer = $(this).closest(".offer-box");
		var $offerDetails = $offerContainer.find(".offer-details");
		var $offerSummary = $offerContainer.find(".offer-summary");
		var $vDivider=$offerContainer.find(".v-divide");
		if (_expandand) {
			$offerContainer.attr("class", offerBoxCSS);
			$offerDetails.attr("class", offerDetailsOld);
			$offerSummary.attr("class", offerSummaryOld);
			$offerDetails.addClass("hide");
			$vDivider.addClass("hide");
		} else {
			$offerContainer.attr("class", offerBoxNew);
			$offerDetails.attr("class", offerDetailsNew);
			$offerSummary.attr("class", offerSummaryNew);
			$offerDetails.removeClass("hide");
			$vDivider.removeClass("hide");
		}
		_expandand = !_expandand;
	});

	$("#policytoggle").click(function(ev){
		ev.preventDefault();
		ev.stopPropagation();
		var isUp = $(this).hasClass("fa-angle-up");
		var $policybody =$("#policybody");
		if(isUp){
			$(this).removeClass("fa-angle-up").addClass("fa-angle-down");
			$policybody.removeClass("hide");
		}else{
			$(this).removeClass("fa-angle-down").addClass("fa-angle-up");
			$policybody.addClass("hide");
		}
	});
});