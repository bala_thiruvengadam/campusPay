var addArrowsProgress = function(color, noofArrows) {
	var sheet = document.styleSheets[0];
	var cssRules = [];


	var spanStyleFn = function(spanId) {
		var templateStr = `#spanarrow_${spanId} {
   -webkit-animation-name: spanarrow_${spanId}; /* Safari 4.0 - 8.0 */
    -webkit-animation-duration: 6s; /* Safari 4.0 - 8.0 */
    -webkit-animation-iteration-count: infinite; /* Safari 4.0 - 8.0 */
  	animation-name: firstarrow;
 	 animation-duration: 6s;
 	animation-iteration-count: infinite;

	}`
		return templateStr;
	};

	var spanAnimationStyleFn = function(spanId, startOpactity) {
		var templateAnimation = `@keyframes spanarrow_${spanId} {`;
		var templateAnimationWebkit = `@-webkit-keyframes spanarrow_${spanId} {`;
		var opacityDelta = 1 / (noofArrows);
		var percentDelta = 100 / (noofArrows);
		for (var i = 0; i < noofArrows; i++) {
			templateAnimation += "" + (i * percentDelta).toFixed(2) + "% {opacity : " + (((startOpactity + (i * opacityDelta)) % 1).toFixed(2)) + "}";
			templateAnimationWebkit += "" + (i * percentDelta).toFixed(2) + "% {opacity : " + (((startOpactity + (i * opacityDelta)) % 1).toFixed(2)) + "}";
		}
		templateAnimation += "100% {opacity : " + startOpactity + "}";
		templateAnimationWebkit += "100% {opacity : " + startOpactity + "}";;

		templateAnimation += "}";
		templateAnimationWebkit += "}";
		return [templateAnimation, templateAnimationWebkit];
	}

}
$(document).ready(function() {

});