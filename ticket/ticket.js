var mongoose = require('../config/mongoose').Mongoose;

var Schema = mongoose.Schema;

var TicketSchema = new Schema({
	ticketId: {
		type: String,
		unique: true
	},
	subject: {
		type: String,
		require: [true, "Ticket subject missing"]
	},
	message: {
		type: String,
		required: [true, "description missing"]
	},
	userid: {
		type: String,
		required: [true, "User Id missing"]
	},
	status: {
		type: String,
		enum: ['OPENED', 'PENDING', 'CLOSED', 'REJECTED'],
		default: 'OPENED'
	},
	email: {
		type: String,
		required: [true, "email required"]
	},
	created_on: {
		type: Date,
		default: Date.now
	},
	updated_on: {
		type: Date,
		default: Date.now
	}
});

module.exports = mongoose.model('Ticket', TicketSchema);