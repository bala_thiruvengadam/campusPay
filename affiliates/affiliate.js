var mongoose = require('../config/mongoose').Mongoose;
var Schema = mongoose.Schema;

var AffSchema = new Schema({
	affiliateId: {
		type: String,
		required: [true, "affiliateId missing"],
		unique: [true, "affiliate is already registered"]
	},
	name: {
		type: String,
		required: [true, "name missing"],
		unique: [true, "affiliate is already registered"]

	},
	website: String,
	logoURL: String,
	created_on: {
		type: Date,
		default: Date.now
	},
	affBuilder: String,
	trkParam: {
		name: {
			type: String,
			require: [true, "merchant tracking parameter name missing"]
		},
		value: {
			type: String,
			require: [true, "merchant tracking parameter value missing"]
		},
		target: {
			type: String,
			enum: ['HEADER', 'PARAM','BODY'],
			default: 'PARAM'
		}
	},
	userTrkParam: {
		name: {
			type: String,
			require: [true, "user  tracking parameter name missing"]
		},
		value: String
	},
	status: {
		type: Boolean,
		default: false
	},
	apis: {

	},
	affTerms: [],
	ensureCashbackMsg: {
		type: String,
		default: "After shopping, cashback will take normally 24 to 72 hours to reflect in your account"
	},
	otherParams: Schema.Types.Mixed
});

module.exports = mongoose.model('Affiliate', AffSchema);