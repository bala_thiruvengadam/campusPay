var dbUtil = require('../config/dbUtil');
var Affiliate = require('./affiliate');

var tableName = "affiliates";
module.exports.getAffsConfigs = function() {
	return new Promise(function(resolve, reject) {
		dbUtil.getConnection(function(db) {
			var affiliatesColl = db.collection(tableName);
			affiliatesColl.find({}).toArray(function(err, affs) {
				if (err) {
					reject(err);
				} else {
					resolve(affs);
				}
			});
		});
	});
};

module.exports.getAffsByName = function(affName) {
	console.log("Affiliate Name :: ", affName);
	return new Promise(function(resolve, reject) {
		if (!affName) {
			reject("aff_name must not be null");
		}
		dbUtil.getConnection(function(db) {
			var affiliatesColl = db.collection(tableName);
			affiliatesColl.findOne({
				affiliateId: affName
			}, function(err, aff) {
				if (err) {
					reject(err);
				} else {
					resolve(aff);
				}
			});
		});
	});
};


module.exports.findOne = function(queryObj) {
	//console.log("Affiliate Name :: ", affName);
	return new Promise(function(resolve, reject) {

		dbUtil.getConnection(function(db) {
			var affiliatesColl = db.collection(tableName);
			affiliatesColl.findOne(queryObj, function(err, aff) {
				if (err) {
					reject(err);
				} else {
					resolve(aff);
				}
			});
		});
	});
};

module.exports.saveAff = function(affObj) {
	console.log("Affiliate Name :: ", affObj.name);
	return new Promise((resolve, reject) => {
		var aff = new Affiliate(affObj);
		aff.save((err, result) => {
			if (err)
				reject(err);
			else {
				resolve(result);
			}
		});
	});

	/*return new Promise(function(resolve, reject) {
		if (!affName) {
			reject("aff_name must not be null");
		}
		dbUtil.getConnection(function(db) {
			var affiliatesColl = db.collection("affiliates_master");
			affiliatesColl.findOne({
				name: affName
			}, function(err, aff) {
				if (err) {
					reject(err);
				} else {
					resolve(aff);
				}
			});
		});
	});*/
};