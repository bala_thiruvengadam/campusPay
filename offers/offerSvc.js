var dbUtil = require('../config/dbUtil');
var async = require('async');
var Offer = require('./Offer');
var async = require("async");
var utils = require('../utils/utils');

var tableName = "offers";
module.exports.updateOffer = function(queryObj, offerObj, options) {
	options = options || {};

	return new Promise((resolve, reject) => {
		if (!offerObj) {
			reject("updateObj must not be null");
		}
		dbUtil.getConn().then((db) => {
			var coll = db.collection(tableName);
			coll.update(queryObj, {
				$set: offerObj
			}, options, function(err, result) {
				if (err) {
					reject(err);
				} else {
					resolve(result);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
};

module.exports.saveOffer = function(offerObj) {
	return new Promise((resolve, reject) => {
		if (!validateOffer(offerObj)) {
			reject("Invalid Offer");
		}
		var offer = new Offer(offerObj);


		async.retry(function(callback) {
			offer.offerId = utils.generateRandom(5);
			offer.save((err, result) => {
				if (err) {
					callback(err);
				} else {
					callback(null, offer);
				}
			});
		}, function(err, result) {
			if (err) {
				reject(err);
			} else {
				resolve(result);
			}
		});

	});
};
var getActiveOffers = function() {
	return new Promise((resolve, reject) => {
		dbUtil.getConn().then((db) => {
			var coll = db.collection(tableName);
			var currentDate = new Date();
			var queryObj = {
				startDate: {
					$lte: currentDate
				},
				endDate: {
					$gte: currentDate
				}
			};
			coll.find(queryObj).toArray(function(err, campaigns) {
				if (err) {
					reject(err);
				} else {
					resolve(campaigns);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
};
module.exports.getActiveOffers = getActiveOffers;

module.exports.getOffer = function(offerId) {
	return new Promise((resolve, reject) => {
		if (!offerId) {
			reject("Offer Id must not be null");
		}
		dbUtil.getConn().then((db) => {
			var coll = db.collection(tableName);
			coll.findOne({
				offerId: offerId
			}, (err, offer) => {
				if (err) {
					reject(err);
				} else {
					resolve(offer);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
};

module.exports.getActiveForAffiliates = function() {

	return new Promise((resolve, reject) => {
		getActiveOffers().then((offers) => {
			async.groupBy(offers, (offer, callback) => {
				callback(null, offer.affiliateId);
			}, (err, groupedOffers) => {
				resolve(groupedOffers);
			});
		}).catch((err) => {
			reject(err);
		});
	});
};


var validateOffer = function(offerObj) {
	if (!offerObj || !offerObj.description || !offerObj.name || !offerObj.campaignURL || !offerObj.affiliateId || !offerObj.startDate || !offerObj.endDate)
		return false;
	return true;
}