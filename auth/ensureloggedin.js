module.exports = function(options) {
	return function(req, res, next) {
		// Implement the middleware function based on the options object

		if (req.user) {
			next();
		} else {
			if (options && options.redirectURL) {
				res.redirect(redirectURL);
			} else {
				res.status(401).json({
					status: "error",
					message: "Unauthenticated request"
				});
			}
		}

	};
}