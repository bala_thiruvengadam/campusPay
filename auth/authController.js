var express = require('express'),
	bcryptUtils = require('../utils/utils'),
	dbUtil = require('../config/dbUtil'),
	walletSvc = require('../wallet/walletSvc'),
	passport = require('../auth/initPassport').passport,
	ensureLoggedIn = require('../auth/ensureloggedin');

var Fawn = require('../config/Fawn').Fawn;


var authRouter = express.Router({
	mergeParams: true
});

module.exports.controller = function(app) {
	app.use("/auth", authRouter);

	authRouter.post("/login/krypto", passport.authenticate('student'), function(req, res) {
		//authentication success
		var sessionUser = req.session.passport.user;
		dbUtil.getConn().then(function(db) {
			var coll = db.collection("users");
			coll.findOne({
				userid: sessionUser.userid
			}, function(err, data) {
				console.log("Login success");
				var trk_id;
				if (data && data.trk_id) {
					trk_id = data.trk_id;

					res.json(data);
				} else {
					saveOrUpdateUserRetry(sessionUser, 0, 5, req, res);

				}

			});
		}).catch(function(err) {
			res.status(503).send("Failed to connect to database");
		})
	});

	authRouter.get("/isLoggedIn", ensureLoggedIn(), function(req, res) {
		console.log("req.session.passport.user", req.user);
		var isLoggedIn = false;
		if (req.user) {
			isLoggedIn = true;
		}
		res.json({
			isLoggedIn: isLoggedIn,
			checked_on: new Date()
		});
	});



	authRouter.post("/login/local", passport.authenticate('local'), function(req, res) {
		//authentication success
		var sessionUser = req.session.passport.user;
		dbUtil.getConn().then(function(db) {
			var coll = db.collection("users");
			coll.findOne({
				userid: sessionUser.userid
			}, function(err, data) {
				if (err) {
					res.status(503).send("Something went wrong");
				} else {
					res.json({
						authenticated: true,
						userid: sessionUser._id
					});
				}

			});
		}).catch(function(err) {
			throw new Error("Failed to connect to database");
		})
	});

	authRouter.get("/home", function(req, res) {
		res.send("Welcome to campusPay");
	});

	authRouter.post("/decrypt", function(req, res) {
		var key = req.body.key;
		var val = req.body.target;
		bcryptUtils.decryptAES(key, val).then((deV) => {
			res.json({
				val: deV
			});
		}).catch((err)=>{
			res.status(503).send(err);
		});
	});



	authRouter.post('/register', function(req, res) {
		var name = req.body.name;
		var email = req.body.email;
		var username = req.body.username;
		var password = req.body.password;
		var role = req.body.role;
		var tenant = req.body.tenant;

		if (!username && !password && !role) {
			res.status(403).send("Bad request..");
		} else {
			bcryptUtils.encryptPassword(password).then(function(data) {
				var newUser = {
					name: name,
					email: email,
					username: username,
					role: role,
					tenant: tenant,
					hash: data.hash,
					salt: data.salt
				};

				dbUtil.getConnection(function(db) {
					var collection = db.collection("users");
					collection.insert(newUser, function(err, data) {
						if (err) {
							res.status(503).send("Failed to register");
						} else {
							res.json({
								status: "success",
								message: "User registered successfully"
							});
						}
					});
				});

			}, function(err) {
				res.status(503).send("registration failed");
			});

		}
	});
};


var saveOrUpdateUserRetry = function(user, retryCnt, maxRetry, req, res) {
	if (retryCnt >= maxRetry)
		res.status(503).send("Something went wrong .. ");

	var trkId = bcryptUtils.generateRandom(5);
	user.trk_id = trkId;

	console.log("user", user);
	dbUtil.getConnection(function(db) {
		var usersColl = db.collection("users");
		usersColl.update({
			userid: user.userid
		}, {
			$set: user
		}, {
			upsert: true
		}, function(err, result) {
			if (err) {
				retryCnt = retryCnt + 1;
				saveOrUpdateUserRetry(user, retryCnt, maxRetry, req, res);
			} else {

				user.trk_id = trkId;
				walletSvc.insert({
					userid: user.userid,
					trk_id: trkId,
					balance: 0,
					totalEarnings: 0,
					cashback: 0,
					rewards: 0,
					redeemed: 0,
					tentativePoints: 0
				}).then((wallet) => {
					res.json(user);

				}).catch((err) => {
					console.log("Error:", err);
					res.status(503).send("Something went wrong .. ");
				})
			}
		});
	});

};