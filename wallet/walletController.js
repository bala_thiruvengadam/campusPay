var express = require('express'),
	ensureLoggedIn = require('../auth/ensureloggedin'),
	walletSvc = require('./walletSvc'),
	walletRouter = express.Router({
		mergeParams: true
	});
module.exports.controller = function(app) {

	//trackRouter.use(ensureLoggedIn());
	app.use("/wallet", walletRouter);


	walletRouter.get("/get", function(req, res) {
		var userid = req.query.userid;
		if (!userid) {
			res.status(403).send("Bad request");
		}
		walletSvc.findOne({
			userid: userid
		}).then((wallet) => {
			res.json(wallet);
		}).catch((err) => {
			console.log(err);
			res.status(503).send("Something went wrong");
		});
	});

};